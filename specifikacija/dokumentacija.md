# Dokumentacija

<hr>

## Opis Igre

Dobrodošao u BgChallenge, aplikaciju koja je stvorena kako bi ti pružila nezaboravno iskustvo upoznavanja Beograda iz jedinstvene perspektive! Naš cilj je da ti omogućimo da istražiš ovaj grad na način koji će te oduševiti i ostaviti te bez daha. 

BgChallenge nije samo obična aplikacija – ona je tvoj vodič kroz lavirinte istorije, geografije i intrigantnih mozgalica, sve to uz nezaboravnu avanturu u srcu Beograda. Ukoliko si strastveni istraživač istorijskih činjenica ovog grada, zaljubljenik u tajne njegovih ulica i želiš da testiraš svoje znanje, ovo je pravo mesto za tebe! 

Svaki nivo aplikacije je osmišljen kako bi predstavio mnoštvo izazova, svaki sa svojim jedinstvenim šarmom i intrigantnim zadacima. U igri su četiri nivoa, a svaki nivo je prava mala riznica ne samo znanja o Beogradu, već i veština koje ćeš razvijati tokom igre. 

Cilj? Osvojiti svaki izazov u što kraćem vremenu! Samo najbrži i najpametniji će se uzdići do titule najboljeg poznavatelja Beograda i osvojiti priznanje kao pravi majstor grada. 

Ali nemoj brinuti ako zapneš! Naš tim je tu da ti pruži podršku u vidu korisnih hintova kako bi te gurnuo napred ka rešenju problema. Bez obzira na prepreku, neka ti ova aplikacija pruži uzbudljivo iskustvo upoznavanja Beograda i njegovih tajni na potpuno nov način. 

Uđi u svet BgChallenge-a i doživi Beograd kao nikada pre! Dozvoli da te ova aplikacija vodi kroz vekove, ulice i priče grada koji čeka da bude otkriven. Spakuj se za avanturu – očekuje te nezaboravan doživljaj!

<hr>

## UML: 

### Dijagram klasa: 

![DijagramKlasa](./dijagram_klasa.svg)

### Dijagram slučajeva upotrebe: 

![DijagramSlucajevaUpotrebe](./dijagram_slucaja_upotrebe.png)

<hr>

### Igranje igre

#### Kratak opis: 
Igrač započinje novu igru unutar glavnog menija aplikacije. Upisuje svoje ime i aplikacija započinje igru. Nakon prelaska igrice, pokazuje se rezultat igrača.

#### Akteri:
Igrač

#### Preduslovi:
Aplikacija je otvorena i prikazuje glavni meni.

#### Postuslovi:
Igrač je uspešno završio igru. Aplikacija prikazuje rezultate i omogućava korisniku dalje akcije (npr. ponovno igranje).

#### Osnovni tok događaja:
```
1. Igrač bira dugme "Započni igru" iz glavnog menija.
2. Aplikacija prikazuje igraču tekst za unos imena.
3. Igrač konfiguriše postavke prema svojim željama.
4. Aplikacija inicira novu igru i prikazuje početno stanje igre.
5. Čuva se inicijalno stanje igre i ime igrača.
6. Prelazi se na slučaj upotrebe "Prvi nivo". Po završetku prelazi se na korak 7.
7. Prelazi se na slučaj upotrebe "Drugi nivo". Po završetku prelazi se na korak 8.
8. Prelazi se na slučaj upotrebe "Treći nivo". Po završetku prelazi se na korak 9.
9. Prelazi se na slučaj upotrebe "Četvrti nivo". Po završetku prelazi se na korak 10.
10. Nakon prelaska cele igrice prikazuje se konačni rezultat.
11. Prelazi se na slučaj upotrebe "Čuvanje rezultata".  Po završetku prelazi se na korak 12
12. Aplikacija se vraća u glavni meni.
```

#### Alternativni tokovi:
```
A1: Neočekivani izlaz igrača iz aplikacije. Ukoliko u bilo kojem trenutku dođe do prekida rada aplikacije, 
sve do tada sačuvane informacije se trajno čuvaju, a ostalo se poništava. Slučaj upotrebe se završava.
```

#### Podtokovi: /
#### Specijalni zahtevi: /
#### Dodatne informacije: /

![IgranjeIgre](./slike/igranje_igre.svg)


<hr>

### Prvi nivo
**Kratak opis**: Igrač na početku otvara prvi nivo, i dobija šest izazova koje sve treba da uradi tačno, kako bi na kraju dobio informaciju o novo otključanom nivou i prelasku na njega.

**Akteri**: Igrač - osoba koja rešava izazove u okviru prvog nivoa

**Preduslovi**: Otvoren je i pokrenut prozor za igranje Prvog nivoa.

**Postuslovi**: Nakon što igrač završi Prvi nivo, prelazi se na Drugi nivo i podaci se ažuriraju.

**Osnovni tok**:

     1. Aplikacija daje pravo igraču da započne igranje prvog nivoa
     2. Aplikacija otvara prvi nivo i niz od šest izazova koje igrač treba da reši
     3. Otvara se prvi izazov
      3.1 Igrač dobija hanojske kule koje treba da složi
       3.1.1 Ako igrač uspešno složi mozgalicu, prelazi na korak 3.2
      3.2 Prikaz edukativnog sadržaja
      3.3 Prikaz asocijacije za sledeću lokaciju
       3.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     4. Otvara se drugi izazov
      4.1 Igrač dobija kviz koji treba da reši
       4.1.1 Ako igrač uspešno složi hanojske kule, prelazi na korak 4.2
      4.2 Prikaz edukativnog sadržaja
      4.3 Prikaz asocijacije za sledeću lokaciju
       4.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     5. Otvara se trći izazov
      5.1 Igrač dobija reč koju treba da speluje morzeovim slovima
       5.1.1 Ako igrač uspešno sepluje zadatu reč, prelazi na korak 5.2
      5.2 Prikaz edukativnog sadržaja
      5.3 Prikaz asocijacije za sledeću lokaciju
       5.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     6. Otvara se četvrti izazov
      6.1 Igrač dobija sliku koju treba da upotpuni
       6.1.1 Ako igrač uspešno reši kviz, prelazi na korak 6.2
      6.2 Prikaz edukativnog sadržaja
    
     7. Aplikacija ažurira trenutno postignuti rezultat
     8. Prelazak na slučaj upotrebe Drugi nivo

**Alternativni tok**:
* A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj upotrebe se završava.

**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: Tokom igranja prvog nivoa, aplikacija pamti rezultat koji je igrač postigao.

<hr>

### Drugi nivo

**Kratak opis**: Igrač nakon završenog prvog nivoa, dobija mogućnost da pređe na drugi nivo i da mu se time otvore novih šest izazova koje treba sve da uradi tačno, kako bi na
kraju dobio informaciju o novo otključanom nivou i prelasku na njega.

**Akteri**: Igrač - osoba koja rešava izazove u okviru drugog nivoa

**Preduslovi**: Igrač je uspešno završio prvi nivo

**Postuslovi**: Nakon što igrač završi drugi nivo, prelazi se na treći nivo i podaci se ažuriraju.

**Osnovni tok**:
    
     1. Otvara se prvi izazov
      1.1 Igrač dobija da poveže imena kultnih vozova sa njihovim destinacijama
       1.1.1 Ako igrač uspešno složi reč, prelazi na korak 1.2
      1.2 Prikaz edukativnog sadržaja
      1.3 Prikaz asocijacije za sledeću lokaciju
       1.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     2. Otvara se drugi izazov
      2.1 Igrač dobija igricu "Lost in migration" koju mora da reši u što kraćem vremenu
       2.1.1 Ako igrač uspe da reši igricu, prelazi na korak 2.2.
      2.2 Prikaz edukativnog sadržaja
      2.3 Prikaz asocijacije za sledeću lokaciju
       2.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     3. Otvara se trći izazov
      3.1 Igrač dobija igru memorije
       3.1.1 Ako igrač uspešno pogodi cene, prlazi na korak 3.2.
      3.2 Prikaz edukativnog sadržaja
      3.3 Prikaz asocijacije za sledeću lokaciju
       3.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
    
     4. Otvara se četvri izazov
      4.1 Igrač dobija dobija slova koja treba da permutira, kako bi došao do tačnog rešenja
       4.1.1 Ako igrač uspešno složi knjige, prelazi na korak 4.2.
      4.2 Prikaz edukativnog sadržaja

     5. Aplikacija ažurira postignuti rezultat
     6. Prelazak na slučaj upotrebe Treći nivo

**Alternativni tok**:
* A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj
 	upotrebe se završava.
	 
**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: Tokom igranja drugog nivoa, aplikacija pamti rezultat koji je igrač postigao.

<hr>

### Treći nivo

**Kratak opis**: Igrač nakon završenog prvog i drugo nivoa, igra treći nivo i dobija novih 6 lokacija. Korisniku nisu unapred poznate lokacije kojim će se kretati, poznata mu je samo početna lokacija, ostale su zaključane. 
Svaka lokacija sadrži izazov. Nakon rešenog izazova, korisnik dobija šifru koju treba da unese u odgovarajuće polje.
Ukoliko je uneta šifra dobra, korisniku se otključavaju dve stvari: edukativni tekst i slika koja predstavlja asocijaciju za sledecu lokaciju. 
Korisnik rešenje te asocijacije treba da unese u odgovarajuće polje. Tačno rešenje otključava narednu lokaciju.
Tako igrač napreduje: lokacija -> izazov -> edukativni tekst, slika -> sledeca lokacija
Ukoliko korisnik to želi, može kliknuti dugme Hint da bi mu se ukazala neka pomoć.

**Akteri**: Igrač - osoba koja rešava izazove u okviru trećeg nivoa

**Preduslovi**: Igrač je uspešno završio prvi i drugi nivo

**Postuslovi**: Nakon što igrač završi treći nivo, prelazi se na četvrti nivo i podaci se ažuriraju.

**Osnovni tok:**

```
 1. Otvara se prvi izazov
  1.1 Igrač dobija ključeve koje treba da, na osnovu tragova koji su mu priloženi, posloži na odgovarajuća mesta
   1.1.1 Ako igrač uspešno poslaže ključeve, pređi na korak 1.2
  1.2 Prikaz edukativnog sadržaja
  1.3 Prikaz asocijacije za sledeću lokaciju
   1.3.1. Tačno rešenje asocijacije se unosi u odgovarajuće polje i naredna lokacija se otključava.

 2. Otvara se drugi izazov
  2.1 Igraču se prikazuje prozor: slika sa skrivenim objektima i lista objekata koji treba da se nađu
   2.1.1 Kada igrač pronađe sve skrivene objekte, otkriva mu se šifra za trenutnu lokaciju. 
  2.2 Prikaz edukativnog sadržaja
  2.3 Prikaz asocijacije za sledeću lokaciju
   2.3.1. Tačno rešenje asocijacije se unosi u odgovarajuće polje i naredna lokacija se otključava.

 3. Otvara se treći izazov
  3.1 Igraču se prikazuju slike 6 svetaca, njegov zadatak je da ih imenuje. 
   3.1.1 Ako igrač uspešno pogodi imena svih svetaca, otkriva mu se šifra za trenutnu lokaciju.
  3.2 Prikaz edukativnog sadržaja
  3.3 Prikaz asocijacije za sledeću lokaciju
   3.3.1. Tačno rešenje asocijacije se unosi u odgovarajuće polje i naredna lokacija se otključava.

 4. Otvara se četvri izazov
  4.1 Igraču se prikazuje slike sa notnom lestvicom i 6 dugmadi. Svako dugme odgovara jednoj noti. Na osnovu tragova koji su mu dati, korisnik treba da pritisne odgovarajuće dugmiće.
   4.1.1 Ako igrač pogodi redosled dugmića, otkriva mu se šifra za trenutnu lokaciju.
  4.2 Prikaz edukativnog sadržaja
  4.3 Prikaz asocijacije za sledeću lokaciju
   4.3.1. Tačno rešenje asocijacije se unosi u odgovarajuće polje i naredna lokacija se otključava.

 5. Aplikacija ažurira postignuti rezultat
 6. Prelazak na slučaj upotrebe Četvrti nivo
```
**Alternativni tok**:
 * A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj
     upotrebe se završava.
     
**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: Tokom igranja trećeg nivoa, aplikacija pamti rezultat koji je igrač postigao.

<hr>

### Četvrti nivo

**Kratak opis:** Igrač nakon završenog trećeg nivoa, dobija mogućnost da pređe na četvrti nivo tj. završni i time kompletira celu sliku o Beogradu. Unutar četvrtog nivoa nalaze se izazovi, koje trebaš rešiti kako bi dobio titulu najboljeg poznavatelja grada.

**Akteri:** Igrač - osoba koja rešava izazove u okviru četvrtog nivoa

**Preduslovi:** Igrač je uspešno završio četvrti nivo

**Postuslovi:** Nakon završenog četvrtog nivoa, podaci se ažuriraju i prikazuju se ostvareni rezultati

**Osnovni tok:**

```
1. Otvara se prvi izazov
 1.1 Igrač dobija zadatak da pronadje uljeza
  1.1.1 Ako igrač uspešno reši, prelazi na korak 1.2
 1.2 Prikaz edukativnog sadržaja
 1.3 Prikaz asocijacije za sledeću lokaciju
  1.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije

2. Otvara se drugi izazov
 2.1 Igrač dobija zadatak da izvrsi tacnu permutaciju slova  u reci
  2.1.1 Ako igrač uspešno reši, prelazi na korak 2.2
 2.2 Prikaz edukativnog sadržaja
 2.3 Prikaz asocijacije za sledeću lokaciju
  2.3.1 Unošenje tačnog rešenja i otključavanje naredne lokacije

3. Otvara se treći izazov
 3.1 Igrač dobija izazov wordle
  3.1.1 Ako igrač uspešno reši, prelazi na korak 3.2
 3.2 Prikaz edukativnog sadržaja
 3.3 Prikaz asocijacije za sledeću lokaciju
  3.1 Unošenje tačnog rešenja i otklučavanje naredne lokacije

4. Otvara se četvrti izazov
  4.1 Igrač dobija delimično popunjen magični kvadrat, kojeg treba popuniti
    4.1.1 Ako igrač uspešno popuni magični kvadrat, prelazi se na korak 4.2
  4.2 Prikaz edukativnog sadržajaž

5. Aplikacija ažurira postignuti rezultat
6. Prelazak na slučaj upotrebe Prikaz Rezultata

```
**Alternativni tokovi:**
* A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj upotrebe se završava.

**Dodatne informacije:** Tokom igranja četvrtog nivoa, aplikacija pamti rezultat koji je igrač postigao.


![PrelazenjeNivoa](./dijagram_sekvence_prelazenje_nivoa.svg)

<hr>

### Hanojske kule

**Kratak opis:** Pred igračom se nalazi mini-igra Hanojske kule. Cilj je prebaciti sve diskove sa pvog stupića na poslednji stubić držeći se redosleda, u što kraćem broju koraka.

**Akteri:** Igrač - osoba koja rešava mini-igru

**Preduslovi:** Igrač je uspešno rešio prethodni izazov

**Postuslovi:** Nakon završene mini-igre, ažurira se rezultat i prelazi se na sledeću mini-igru

**Osnovni tok:**

```
1. Prikazivanje početnog stanja mini-igre
2. Postupak premestanja diskova tako da poslednji stupic izgleda kao prvi
 2.1 Premeštanje najmanjeg diska na drugi stubić
 2.2 Premeštanje srednjeg diska na treći stubić
 2.3 Premeštanje najmanjeg diska na treći stubić
 2.4 Premeštanje najvećeg diska na drugi stubić
 2.5 Premeštanje najmanjeg diska na drugi stubić
 2.6 Premeštanje srednjeg diska na prvi stubić
 2.7 Premeštanje najmanjeg diska na prvi stubić
 2.8 Premeštanje najvećeg diska na treći stubić
 2.9 Premeštanje najmanjeg diska na drugi stubić
 2.10 Premeštanje srednjeg diska na treći stubić
 2.11 Premeštanje najmanjeg diska na treći stubić
  * Ukoliko je potrebna pomoć, igrač zatražuje hint
   ** Prikazivanje sugestije za sledeći korak   
3. Prikaz edukativnog teksta
4. Prikaz asocijacije za sledeću lokaciju
 4.1 Unošenje tačnog rešenja i otključavanje naredne lokacije
5. Prelazak na sledeću mini-igru

```

**Alternativni tok:**
* A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj upotrebe se završava.

**Podtokovi:** / 

**Specijalni zahtevi:** /

**Dodatne informacije:** Tokom igranja mini-igre, aplikacija ažurira postignute rezultate.

![HanojskeKule](./dijagram_sekvence_hanojske_kule.svg)

<hr>

### MATF

**Kratak opis:** Pred igračom se nalazi mini-igra MATF u kome treba da se reši magični kvadrat. Magični kvadrat je kvadrat u kome je zbir svih elemenata na dijagonalama, redovima i kolonama jednak zbiru 15.

**Akteri:** Igrač - osoba koja rešava mini-igru

**Preduslovi:** Uspešno je završena prethodna mini-igra

**Postuslovi:** Nakon završenog izazova ažurira se rezultat i prikazuju se postignuti rezultati osvojeni tokom celog igranja igre

**Osnovni tok:**

```
1. Iscrtavanje polupopunjenog magičnog kvadrata
2. Popunjavanje kvadrata
 2.1 Popunjavanje prve vrste
  2.1.1 Unošenje jednocifrenog broja na poziciju [1,2]
  2.1.2 Unošenje jednocifrenog broja na poziciju [1,3]
 2.2 Popunjavanje druge vrste
  2.2.1 Unošenje jednocifrenog broja na poziciju [2,1]
 2.3 Popunjavanje treće vrste
  2.3.1 Unošenje jednocifrenog broja na poziciju [3,1]
  2.3.2 Unošenje jednocifrenog broja na poziciju [3,2]
 2.4 Ukoliko je potrebna pomoć, pritiska se dugme `Hint`
3. Proveravanje da li je dati kvadrat u ispravnom formatu, pritiskom na dugme `Provera`
 3.1 Ako je u pogrešnom formatu, prelazi se na korak 2.
 3.2 Ako je u ispravnom formatu, prelazi se na korak 4.
4. Prikazivanje edukativnog teksta
5. Aplikacija ažurira postignuti rezultat
6. Prelazak na slučaj upotrebe Prikaz rezultata
```
**Alternativni tok:**
* A1: Neočekivani izlaz iz aplikacije. Ako korisnik u bilo kom trenutku isključi aplikaciju, sve zapamćene informacije se sačuvavaju i aplikacija završava rad. Slučaj upotrebe se završava.

**Podtokovi:** /

**Specijalni zahtevi:** /

**Dodatne informacije:** Tokom igranja mini-igre, aplikacija ažurira postignute rezultate.


![MATF](./dijagram_sekvence_matf.svg)

<hr>

### Čuvanje rezultata

**Kratak opis**: Prilikom kompletiranja igrice ili uspešno savladanog _Challenge_-a, čuvaju se informacije o rezultatima igrača.

**Akteri**: /

**Preduslovi**: Igrač je uspešno savladao sve nivoe igrice, ili je uspešno završio jedan _Challenge_.

**Postuslovi**: Podaci o trenutnom stanju igre za datog igrača su trajno sačuvani.

**Osnovni tok**:
```
1. Aplikacija serijalizatoru podataka šalje informacije o igraču i trenutnom stanju igrice
2. Podaci se serijalizuju
3. Serijalizovani podaci se skladište u fajl sistemu
4. Nastavlja se tok Igranje igre
```
**Aletarnativni tokovi**:
```
A1: Neočekivani zlaz iz aplikacije. Ukoliko u bilo kojem koraku dođe do prekida rada aplikacije, zapamćeni rezultati neće biti sačuvani. Slučaj upotrebe se završava.
A2: Igrač je kompletirao igricu. Ako je igrač savladao sve nivoe, u koraku 4. se prikazuje Tabela sa rezultatima. Slučaj upotrbe se završava.
```
**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: Tokom igranja drugog nivoa, aplikacija pamti rezultat koji je igrač postigao.

![CuvanjeRezultata](./slike/cuvanje_rezultata.svg)

<hr>

### Nastavljanje igre

#### Kratak opis: 
Igrač nastavlja prethodno sačuvanu igru.

#### Akteri:
Igrač

#### Preduslovi:
Postoji prethodno sačuvana igra.

#### Postuslovi:
Igrač uspesno nastavlja igru.

#### Osnovni tok događaja:
```
1. Igrač bira opciju "Nastavi igru" iz glavnog menija.
2. Aplikacija prikazuje listu sačuvanih igara.
3. Igrač bira željenu sačuvanu igru.
4. Aplikacija učitava prethodno sačuvano stanje igre i učitava staru mapu.
5. Igrica se nastavlja iz tog stanja.
```

#### Alternativni tokovi:
```
A1: Neočekivani izlaz igrača iz aplikacije. Ukoliko u bilo kojem trenutku dođe do prekida rada aplikacije, 
sve do tada sačuvane informacije se trajno čuvaju, a ostalo se poništava. Slučaj upotrebe se završava.
```

#### Podtokovi: /
#### Specijalni zahtevi: /
#### Dodatne informacije: /

![NastavljanjeIgre](./slike/nastavi_igru.svg)

<hr>

### Prikaz rezultata

**Kratak opis**: Aplikacija prikazuje poene igrača koje je ostvario u partiji.

**Akteri**: Igrač

**Preduslovi**: Završeni su svi nivoi.

**Postuslovi**: Aplikacija prikazuje glavni meni.

**Osnovni tok**:  

      1. Igrač pritiska dugme iz glavnog menija kako bi se prikazali rezultati.  
      2. Aplikacija prikazuje prozor na kome se u obliku tabele prikazuju rezultati.  
      3. Igrač pritiska dugme koje ga vraća na glavni meni. 
      4. Aplikacija prikazuje glavni meni. 

**Alternativni tokovi**: 
* A1: Neočekivani izlaz iz aplikacije: Ako u bilo kom koraku korisnik prekine aplikaciju, sve zapamćene informacije o nivoima se sačuvavaju i aplikacija završava rad. Slučaj upotrebe je završen.

**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: /	

![PrikazRezultata](./slike/prikaz_rezultata.svg)

<hr>

#### Slučaj upotrebe: Podešavanje

#### Kratak opis:
Igrac bira opciju Podešavanje iz glavnog menija aplikacije. Izborom te opcije
otvara se prozor u kome se mogu podesiti zvuk I ton klika.


#### Akteri:
- Igrač
#### Preduslovi:
- Aplikacija je pokrenuta I prikazan je glavni meni.

#### Osnovni tok:
    1. Igrac bira dugme "Podešavanje" iz glavnog menija.
    2. Aplikacija prikazuje prozor sa opcijama koje se mogu izmeniti.
    3. Ako je igrač izabrao opciju podešavanje jačine zvuka
       3.1. Ako je izabrao odredjenu opciju
          3.1.1. Izabrana opcija se primenjuje do kraja igre
       3.2. Inače se primenjuju podrazumevana podešavanja za jačinu zvuka
    4. Ako je igrač izabrao opciju podešavanje tona klika
       4.1. Ako je izabrao odredjenu opciju
          4.1.1. Izabrana opcija se primenjuje do kraja igre
       4.2. Inače se primenjuju podrazumevana podesavanja za ton klika
    5. Igrac bira dugme "Nazad" za povratak u glavni meni

#### Alternativni tokovi:
- A1: Neočekivani izlaz iz aplikacije. Ako prilikom podešavanja igre,
korisnik iskljuci aplikaciju, sve izabrane opcije podešavanja se brisu I
aplikacija završava rad. Slučaj upotrebe se zavrsava.

#### Podtokovi: /
#### Specijalni zahtevi: /
#### Dodatne informacije:/


![Podesavanja](./slike/podesavanja.svg)


