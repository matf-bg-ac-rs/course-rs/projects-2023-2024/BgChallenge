# Slučaj upotrebe: Nastavak igre

### Kratak opis: 
Igrač nastavlja prethodno sačuvanu igru.

### Akteri:
Igrač

### Preduslovi:
Postoji prethodno sačuvana igra.

### Postuslovi:
Igrač uspesno nastavlja igru.

### Osnovni tok događaja:
```
1. Igrač bira opciju "Nastavi igru" iz glavnog menija.
2. Aplikacija prikazuje listu sačuvanih igara.
3. Igrač bira željenu sačuvanu igru.
4. Aplikacija učitava prethodno sačuvano stanje igre i učitava staru mapu.
5. Igrica se nastavlja iz tog stanja.
```

### Alternativni tokovi:
```
A1: Neočekivani izlaz igrača iz aplikacije. Ukoliko u bilo kojem trenutku dođe do prekida rada aplikacije, 
sve do tada sačuvane informacije se trajno čuvaju, a ostalo se poništava. Slučaj upotrebe se završava.
```

### Podtokovi: /
### Specijalni zahtevi: /
### Dodatne informacije: /