#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "catch.hpp"
#include "../include/pronadjiuljeza.h"
#include "../forms/ui_pronadjiuljeza.h"
#include "../include/Challenge_test.h"
#include "../include/timer.h"
#include <QApplication>


class PronadjiUljezaTest {
public:
    static Ui::PronadjiUljeza* getUi(PronadjiUljeza& pu) {
        return pu.ui;
    }
    static void znakpitanjaClicked(PronadjiUljeza& pu) {
        return pu.znakpitanja_clicked();
    }
    static void zlatnibokalClicked(PronadjiUljeza& pu) {
        return pu.zlatnibokal_clicked();
    }
    static void trisesiraClicked(PronadjiUljeza& pu) {
        return pu.trisesira_clicked();
    }
    static void malivrabacClicked(PronadjiUljeza& pu) {
        return pu.malivrabac_clicked();
    }
    static void sesirmojClicked(PronadjiUljeza& pu) {
        return pu.sesirmoj_clicked();
    }
    
};

TEST_CASE("Pronadji Uljeza Testovi", "[PronadjiUljeza]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    SECTION("Inicijalizacija"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        REQUIRE(PronadjiUljezaTest::getUi(pronadjiUljeza) != nullptr);
    }

    SECTION("Provera da li se izvrsava QMessageBox kada se klikne na znakpitanja"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        pronadjiUljeza.znakpitanja = false;
        PronadjiUljezaTest::znakpitanjaClicked(pronadjiUljeza);

        REQUIRE(pronadjiUljeza.znakpitanja == true);
    }
    SECTION("Provera da li se izvrsava QMessageBox kada se klikne na zlatnibokal"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        pronadjiUljeza.zlatnibokal = false;
        PronadjiUljezaTest::zlatnibokalClicked(pronadjiUljeza);

        REQUIRE(pronadjiUljeza.zlatnibokal == true);
    }
    SECTION("Provera da li se izvrsava QMessageBox kada se klikne na trisesira"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        pronadjiUljeza.trisesira = false;
        PronadjiUljezaTest::trisesiraClicked(pronadjiUljeza);

        REQUIRE(pronadjiUljeza.trisesira == true);
    }
    SECTION("Provera da li se izvrsava QMessageBox kada se klikne na malivrabac"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        pronadjiUljeza.malivrabac = false;
        PronadjiUljezaTest::malivrabacClicked(pronadjiUljeza);

        REQUIRE(pronadjiUljeza.malivrabac == true);
    }

    SECTION("Provera da li se izvrsava QMessageBox kada se klikne na sesirmoj"){
        Timer timer;
        int pocetnoVreme = 0;
        PronadjiUljeza pronadjiUljeza(&timer, pocetnoVreme, nullptr);

        pronadjiUljeza.sesirmoj = false;
        PronadjiUljezaTest::sesirmojClicked(pronadjiUljeza);

        REQUIRE(pronadjiUljeza.sesirmoj == true);
    }




}
