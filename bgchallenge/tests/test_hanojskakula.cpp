#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "catch.hpp"
#include "../include/hanojskakula.h"
#include "../include/Challenge_test.h"
#include "../include/timer.h"
#include <QApplication>
#include <QGraphicsPixmapItem>
#include <QDebug>

class HanojskakulaTest {
public:
    static void on_RetryClicked(Hanojskakula& h) {
        h.on_RetryClicked();
    }
    static void callPomoc(Hanojskakula& h) {
        h.pomoc();
    }
    static QGraphicsPixmapItem* getDisk1(Hanojskakula& h){
        return h.disk1Item;
    }
    static QGraphicsPixmapItem* getDisk2(Hanojskakula& h){
        return h.disk2Item;
    }
    static QGraphicsPixmapItem* getDisk3(Hanojskakula& h){
        return h.disk3Item;
    }
    static QStack<unsigned char> getStub1(Hanojskakula& h){
        return h.stub1;
    }

    static QStack<unsigned char> getStub2(Hanojskakula& h){
        return h.stub2;
    }

    static QStack<unsigned char> getStub3(Hanojskakula& h){
        return h.stub3;
    }
};

TEST_CASE("Hanojskakula Testovi", "[Hanojska]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    SECTION("Inicijalizacija") {
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QStack<unsigned char> stub1 = HanojskakulaTest::getStub1(hanojskakula);
        REQUIRE(stub1.size() == 3);
        REQUIRE(stub1.pop() == 1);
        REQUIRE(stub1.pop() == 2);
        REQUIRE(stub1.pop() == 3);
        REQUIRE(HanojskakulaTest::getStub2(hanojskakula).isEmpty());
        REQUIRE(HanojskakulaTest::getStub3(hanojskakula).isEmpty());
        REQUIRE(HanojskakulaTest::getDisk1(hanojskakula)->x() == -100 + 70);
        REQUIRE(HanojskakulaTest::getDisk1(hanojskakula)->y() == 179 - 40);
        REQUIRE(HanojskakulaTest::getDisk2(hanojskakula)->x() == -100 + 54);
        REQUIRE(HanojskakulaTest::getDisk2(hanojskakula)->y() == 179 - 20);
        REQUIRE(HanojskakulaTest::getDisk3(hanojskakula)->x() == -100 + 40);
        REQUIRE(HanojskakulaTest::getDisk3(hanojskakula)->y() == 179);
        REQUIRE(HanojskakulaTest::getDisk1(hanojskakula)->flags() & QGraphicsItem::ItemIsMovable);
        REQUIRE(!(HanojskakulaTest::getDisk2(hanojskakula)->flags() & QGraphicsItem::ItemIsMovable));
        REQUIRE(!(HanojskakulaTest::getDisk3(hanojskakula)->flags() & QGraphicsItem::ItemIsMovable));
    }

    SECTION("Pomeranje diskova") {
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QStack<unsigned char> stub1 = HanojskakulaTest::getStub1(hanojskakula);
        REQUIRE(stub1.size() == 3);
        QStack<unsigned char> stub2 = HanojskakulaTest::getStub2(hanojskakula);
        REQUIRE(stub2.size() == 0);
        stub2.push(stub1.pop());
        REQUIRE(stub2.size() == 1);
        REQUIRE(stub2.top() == 1);
    }

    SECTION("Retry opcija") {
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QStack<unsigned char> stub1 = HanojskakulaTest::getStub1(hanojskakula);
        REQUIRE(stub1.size() == 3);
        QStack<unsigned char> stub2 = HanojskakulaTest::getStub2(hanojskakula);
        REQUIRE(stub2.size() == 0);
        stub2.push(stub1.pop());
        REQUIRE(stub2.size() == 1);
        REQUIRE(stub2.top() == 1);

        HanojskakulaTest::on_RetryClicked(hanojskakula);
        //ovde se testira i metod postaviPocetnoStanje
        REQUIRE(HanojskakulaTest::getStub1(hanojskakula).size() == 3);
        REQUIRE(HanojskakulaTest::getStub2(hanojskakula).isEmpty());
        REQUIRE(HanojskakulaTest::getStub3(hanojskakula).isEmpty());
    }

//    SECTION("Pomoc opcija"){
//        Timer timer;
//        timer.start();
//        Hanojskakula hanojskakula(&timer, 0, nullptr);
//        hanojskakula.pomocExecuted = false;

//        HanojskakulaTest::callPomoc(hanojskakula);
//        REQUIRE(hanojskakula.pomocExecuted);
//    }

    SECTION("PromeniDozvolu metoda sa dozvolom true") {
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QGraphicsPixmapItem* disk2Item = HanojskakulaTest::getDisk2(hanojskakula);

        QStack<unsigned char> stub;
        stub.push(2);

        hanojskakula.promeniDozvolu(stub, true);
        REQUIRE(disk2Item->flags() & QGraphicsItem::ItemIsMovable);
    }

    SECTION("PromeniDozvolu metoda sa dozvolom false") {
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QGraphicsPixmapItem* disk3Item = HanojskakulaTest::getDisk3(hanojskakula);

        QStack<unsigned char> stub;
        stub.push(3);

        hanojskakula.promeniDozvolu(stub, false);
        REQUIRE(!(disk3Item->flags() & QGraphicsItem::ItemIsMovable));
    }

    SECTION("Smanji stek"){
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QStack<unsigned char> stub = HanojskakulaTest::getStub1(hanojskakula);
        QGraphicsPixmapItem* disk2Item = HanojskakulaTest::getDisk2(hanojskakula);

        hanojskakula.smanjiStek(stub);

        REQUIRE(stub.size() == 2);
        REQUIRE(stub.top() == 2);
        REQUIRE(disk2Item->flags() & QGraphicsItem::ItemIsMovable);
    }

    SECTION("Nedozvoljen potez"){
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QGraphicsPixmapItem* disk2Item = HanojskakulaTest::getDisk2(hanojskakula);
        QStack<unsigned char> stub;
        stub.push(1);

        REQUIRE(!(hanojskakula.dozvoljenPotez(disk2Item,stub)));

    }
    SECTION("Dozvoljen potez"){
        Timer timer;
        timer.start();
        Hanojskakula hanojskakula(&timer, 0, nullptr);

        QGraphicsPixmapItem* disk2Item = HanojskakulaTest::getDisk2(hanojskakula);
        QStack<unsigned char> stub;
        stub.push(3);

        REQUIRE(hanojskakula.dozvoljenPotez(disk2Item,stub));

    }

}
