cmake_minimum_required(VERSION 3.5)

project(bgchallenge_test VERSION 0.1)


set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC_SEARCH_PATHS ${CMAKE_CURRENT_SOURCE_DIR}/forms)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)



find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core Widgets Test UiTools Multimedia)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Widgets Test UiTools Multimedia REQUIRED)
find_package(Catch2 REQUIRED)

set(TEST_SOURCE_FILES
    main_test.cpp
    rezultat_test.cpp
    wordle_test.cpp
    morzeova_azbuka_test.cpp
    sveci_test.cpp
    vesalice_test.cpp
    igrac_test.cpp
    ../include/rezultat.h
    ../include/igrac.h
    ../source/rezultat.cpp
    ../source/igrac.cpp
    ../include/timer.h
    ../source/timer.cpp
    ../include/Challenge_test.h
    ../source/Challenge_test.cpp
    ../include/challengeKalemegdan.h
    ../source/challengeKalemegdan.cpp
    ../forms/kalemegdanUI.ui
    ../include/challengeMorzeovaAzbukaUI.h
    ../source/challengeMorzeovaAzbukaUI.cpp
    ../forms/morzeovaAzbuka.ui
    test_hanojskakula.cpp
    ../source/hanojskakula.cpp
    ../forms/hanojskakula.ui
    ../include/hanojskakula.h
    test_kviz.cpp
    ../source/kviz.cpp
    ../forms/kvizUI.ui
    ../include/kviz.h
    test_pronadjiuljeza.cpp
    ../source/pronadjiuljeza.cpp
    ../forms/pronadjiuljeza.ui
    ../include/pronadjiuljeza.h

    lostinmigration_test.cpp
    ../include/lostinmigration.h
    ../source/lostinmigration.cpp
    ../forms/lostinmig.ui
    memorija_test.cpp
    ../include/memorija.h
    ../source/memorija.cpp
    ../forms/memorija.ui

    skriveni_test.cpp
    ../include/skriveni.h
    ../source/skriveni.cpp
    ../forms/skriveni2.ui

    ../include/vesalice.h
    ../source/vesalice.cpp
    ../forms/vesalice.ui

    ../include/sveci.h
    ../source/sveci.cpp
    ../forms/sveci.ui

    ../resources.qrc
    memorija_test.cpp
    skriveni_test.cpp
    lostinmigration_test.cpp

    ../include/memorija.h
    ../source/memorija.cpp
    ../forms/memorija.ui

    ../include/skriveni.h
    ../source/skriveni.cpp
    ../forms/skriveni2.ui

    ../include/lostinmigration.h
    ../source/lostinmigration.cpp
    ../forms/lostinmig.ui

  ../include/puzla.h
    ../source/puzla.cpp
    ../forms/puzla.ui

    ../include/matf.h
    ../source/matf.cpp
    ../forms/matf.ui

    ../include/timer.h
    ../source/timer.cpp

    test_matf.cpp
    test_puzla.cpp
    test_timer.cpp


    )




add_executable(${PROJECT_NAME})

target_sources(${PROJECT_NAME}
    PRIVATE
    ${TEST_SOURCE_FILES}

)

target_link_libraries(${PROJECT_NAME}
    PRIVATE
    Catch2::Catch2
    Qt${QT_VERSION_MAJOR}::Core
    Qt${QT_VERSION_MAJOR}::Widgets
    Qt${QT_VERSION_MAJOR}::Test
    Qt${QT_VERSION_MAJOR}::UiTools
    Qt${QT_VERSION_MAJOR}::Multimedia

)


include(CTest)
include(Catch)
catch_discover_tests(${PROJECT_NAME} EXTRA_ARGS -s)


