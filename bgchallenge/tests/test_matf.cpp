#include "catch.hpp"

#include "../include/matf.h"
#include "../include/timer.h"
#include "../forms/ui_matf.h"
#include<QApplication>
#include<QDebug>

QVector<QVector<int>> matrica(Ui::MATF *ui)
{
    QVector<QVector<int>> m(3, QVector<int>(3));
    int n = m.size();
    bool ok;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            m[i][j] = ui->tableWidget->item(i, j)->text().toInt(&ok);
    }

    return m;
}

bool uporedi(const QVector<QVector<int>> &a, const QVector<QVector<int>> & b)
{
    int n = a.size();
    int m = b.size();

    if (n != m)
        return false;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            if (a[i][j] != b[i][j])
                return false;
    }

    return true;
}

TEST_CASE("Testiranje korektnosti klase MATF", "[matf]")
{
    int argc = 0;
    char **argv{nullptr};

    QApplication app(argc, argv);

    SECTION("Proverava ispravne inicijalizacije konstruktora")
    {
        // arrange
        Timer timer;
        timer.start();
        MATF matf(&timer, 0, nullptr);
        Ui::MATF *ui = matf.ui;

        const auto ocekivaniTitle = "BgChallenge";
        const auto ocekivaniMinWidth = 1280;
        const auto ocekivaniMinHeigh = 720;

        const auto ocekivano00 = "8";
        const auto ocekivano11 = "5";
        const auto ocekivano12 = "9";
        const auto ocekivano22 = "2";

        // act
        const auto title = matf.windowTitle();
        const auto minWidth = matf.minimumWidth();
        const auto minHeigh = matf.minimumHeight();

        const auto item00 = ui->tableWidget->item(0, 0)->text();
        const auto item11 = ui->tableWidget->item(1, 1)->text();
        const auto item12 = ui->tableWidget->item(1, 2)->text();
        const auto item22 = ui->tableWidget->item(2, 2)->text();

        // assert
        REQUIRE(title == ocekivaniTitle);
        REQUIRE(minWidth == ocekivaniMinWidth);
        REQUIRE(minHeigh == ocekivaniMinHeigh);

        REQUIRE(ocekivano00 == item00);
        REQUIRE(ocekivano11 == item11);
        REQUIRE(ocekivano12 == item12);
        REQUIRE(ocekivano22 == item22);

    }

    SECTION("Proveravamo da li je tabela ispravno popunjena")
    {
        // arrange
        Timer timer;
        timer.start();
        MATF matf(&timer, 0, nullptr);
        Ui::MATF *ui = matf.ui;

        const auto ocekivanZbir15 = true;
        QVector<QVector<int>> ocekivanaMatrica = {
            {8, 3, 4},
            {1, 5, 9},
            {6, 7, 2}
        };

        // act
        matf.onPbProveriClicked();
        const auto zbir15 = matf.getCheckSum(8, 3, 4);
        const auto mat = matrica(ui);

        // asserts
        REQUIRE(ocekivanZbir15 == zbir15);
        REQUIRE(uporedi(ocekivanaMatrica, mat));
    }
}
