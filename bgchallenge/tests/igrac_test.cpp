#include "catch.hpp"
#include "../include/igrac.h"  // Promenite putanju ako je potrebno

TEST_CASE("Igrac konstruktori i osnovne funkcije", "[igrac]") {
    // Testiranje konstruktora
    Igrac igrac1;
    REQUIRE(igrac1.getIme().isEmpty());
    REQUIRE(igrac1.getVreme() == 0);
    REQUIRE(igrac1.getNivo() == 1);
    REQUIRE(igrac1.getPoslednjiOtkljucaniChallenge() == 0);

    Igrac igrac2("TestIgrac");
    REQUIRE(igrac2.getIme() == "TestIgrac");
    REQUIRE(igrac2.getVreme() == 0);
    REQUIRE(igrac2.getNivo() == 1);
    REQUIRE(igrac2.getPoslednjiOtkljucaniChallenge() == 0);

    Igrac igrac3("TestIgrac", 100, 2, 3);
    REQUIRE(igrac3.getIme() == "TestIgrac");
    REQUIRE(igrac3.getVreme() == 100);
    REQUIRE(igrac3.getNivo() == 2);
    REQUIRE(igrac3.getPoslednjiOtkljucaniChallenge() == 3);

    // Testiranje set metoda
    igrac1.setPoslednjiOtkljucaniChallenge(5);
    REQUIRE(igrac1.getPoslednjiOtkljucaniChallenge() == 5);

    igrac1.setNivo(3);
    REQUIRE(igrac1.getNivo() == 3);

    igrac1.setVreme(50);
    REQUIRE(igrac1.getVreme() == 50);

    // Ostale funkcije možete testirati na sličan način
}

TEST_CASE("Igrac sacuvaj funkcija", "[igrac]") {
    // Testiranje sacuvaj metode
    Igrac igrac("TestIgrac", 100, 2, 3);

    // Možete koristiti mock klase ili fajlove za testiranje
    // da biste simulirali pisanje i čitanje iz fajla
    // Ovde ćemo samo proveriti da li metoda ne baca izuzetak
    REQUIRE_NOTHROW(igrac.sacuvaj());
}

TEST_CASE("Igrac toJson i fromJson funkcije", "[igrac]") {
    // Testiranje toJson i fromJson metoda
    Igrac igrac("TestIgrac", 100, 2, 3);
    QJsonObject json = igrac.toJson();
    Igrac noviIgrac = Igrac::fromJson(json);

    REQUIRE(igrac.getIme() == noviIgrac.getIme());
    REQUIRE(igrac.getVreme() == noviIgrac.getVreme());
    REQUIRE(igrac.getNivo() == noviIgrac.getNivo());
    REQUIRE(igrac.getPoslednjiOtkljucaniChallenge() == noviIgrac.getPoslednjiOtkljucaniChallenge());
}
