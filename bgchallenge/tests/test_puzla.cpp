#include "catch.hpp"

#include "../include/puzla.h"
#include "../include/timer.h"
#include "../forms/ui_puzla.h"

#include <QApplication>
#include <QPushButton>
#include <QTableWidget>

TEST_CASE("Testiranje korektnosti klase Puzla", "[puzla]")
{
    int argc = 0;
    char **argv = {nullptr};

    QApplication app(argc, argv);

    SECTION("Provera ispravne inicijalizacije konstruktora u klasi Puzla")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);

        const auto ocekivaniTitle = "BgChallenge";
        const auto ocekivaniMinWidth = 1280;
        const auto ocekivaniMinHeigh = 720;

        // act
        const auto title = puzla.windowTitle();
        const auto minWidth = puzla.minimumWidth();
        const auto minHeigh = puzla.minimumHeight();

        // assert
        REQUIRE(ocekivaniTitle == title);
        REQUIRE(ocekivaniMinHeigh == minHeigh);
        REQUIRE(ocekivaniMinWidth == minWidth);
    }


    SECTION("Proveravamo da li je slika1 dato resenje")
    {
        // arrange
        Timer *timer = new Timer();
        timer->start();
        Puzla *puzla = new Puzla(timer, 0, nullptr);
        Ui::Puzla *ui = puzla->ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla->onPbSlika1Clicked();

        const auto disabled = ui->pbSlika1->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);

        delete timer;
        delete puzla;
    }

    SECTION("Proveravamo da li je slika2 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbSlika2Cilcked();

        const auto disabled = ui->pbSlika2->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je slika3 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbSlika3Clicked();

        const auto disabled = ui->pbSlika3->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je slika4 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.obPbSlika4Clicked();

        const auto disabled = ui->pbSlika4->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je slika5 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbSlika5Clicked();

        const auto disabled = ui->pbSlika5->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je slika6 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbSlika6Clicked();

        const auto disabled = ui->pbSlika6->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je missing_piece dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbMissingPieceClicked();

        const auto disabled = ui->pbMissingPiece->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }

    SECTION("Proveravamo da li je v1 dato resenje")
    {
        // arrange
        Timer timer;
        timer.start();
        Puzla puzla(&timer, 0, nullptr);
        Ui::Puzla *ui = puzla.ui;

        const auto ocekivanoDisabled = false;

        // act
        puzla.onPbV1Clicked();

        const auto disabled = ui->pbV1->isEnabled();

        // assert
        REQUIRE(ocekivanoDisabled == disabled);
    }
}
