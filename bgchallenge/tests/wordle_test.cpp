#include "catch.hpp"


#include "../include/timer.h"
#include "../include/Challenge_test.h"
#include "../include/challengeKalemegdan.h"
#include <QApplication>
#include <QSignalSpy>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>



TEST_CASE("Wordle izazov", "[class]"){
    int argc = 0;
    char* argv[] = { nullptr };

    QApplication app(argc, argv);





    SECTION("Reagovanje na signale od sata i ispravno postavljanje pocetnih vrednosti polja")
    {

        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);

        QSignalSpy spy(&timer, SIGNAL(azurirajVreme(int)));

        emit timer.azurirajVreme(1);

        app.processEvents();


        REQUIRE(spy.count() > 0);
        REQUIRE(challenge.getTrenutnaPozicija() == 0);
        REQUIRE(challenge.getPozicijaPocetnogSlova() == 0);
        REQUIRE(challenge.getTrenutnaRec().size() == 5);

    }

    SECTION("Resetovanje stanja")
    {
        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);
        int condition = 0;
        const QVector<QLabel*>& polja = challenge.getPolja();
        challenge.setPozicijaPocetnogSlova(10);
        challenge.setTrenutnaPozicija(13);

        challenge.zapocniChallenge();

        for(int i = 0; i < 30; i++)
        {
            if(polja[i]->text() == "")
                condition++;
        }

        REQUIRE(challenge.getTrenutnaPozicija() == 0);
        REQUIRE(challenge.getPozicijaPocetnogSlova() == 0);
        REQUIRE(challenge.getTrenutnaRec().size() == 5);
        REQUIRE(condition == 30);
    }

    SECTION("Pogodjena rec")
    {
        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);
        QStringList rec = {"l", "e", "m", "u", "r"};
        challenge.setTrenutnaRec(rec);
        QSignalSpy spy(&challenge, SIGNAL(pogodjenaRec()));


        challenge.ui->findChild<QPushButton*>("pbL")->click();
        challenge.ui->findChild<QPushButton*>("pbE")->click();
        challenge.ui->findChild<QPushButton*>("pbM")->click();
        challenge.ui->findChild<QPushButton*>("pbU")->click();
        challenge.ui->findChild<QPushButton*>("pbR")->click();
        challenge.ui->findChild<QPushButton*>("pbPogodi")->click();

        app.processEvents();

        REQUIRE(challenge.getTrenutnaRec() == rec);
        REQUIRE(spy.count() == 1);

    }

    SECTION("Delimicno pogodjena rec")
    {
        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);
        QStringList rec = {"l", "e", "m", "u", "r"};
        challenge.setTrenutnaRec(rec);
        qDebug()<<challenge.getTrenutnaRec();
        int condition = 0;
        const QVector<QLabel*>& polja = challenge.getPolja();
        QRegularExpression regex("background-color:\\s*([^;]+);");
        QRegularExpressionMatch match;
        QString colorString;

        challenge.ui->findChild<QPushButton*>("pbL")->click();
        challenge.ui->findChild<QPushButton*>("pbE")->click();
        challenge.ui->findChild<QPushButton*>("pbM")->click();
        challenge.ui->findChild<QPushButton*>("pbZ")->click();
        challenge.ui->findChild<QPushButton*>("pbK")->click();
        challenge.ui->findChild<QPushButton*>("pbPogodi")->click();

        match = regex.match(polja[0]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;
        if(colorString == "#4fb946")
            condition++;

        match = regex.match(polja[1]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#4fb946")
            condition++;

        match = regex.match(polja[2]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#4fb946")
            condition++;

        match = regex.match(polja[3]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#a0b0c7")
            condition++;

        match = regex.match(polja[4]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#a0b0c7")
            condition++;


        app.processEvents();

        REQUIRE(condition == 5);
        REQUIRE(challenge.getTrenutnaPozicija() == 5);
        REQUIRE(challenge.getPozicijaPocetnogSlova() == 5);



    }

    SECTION("Slova se nalaze u reci")
    {
        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);
        QStringList rec = {"l", "e", "m", "u", "r"};
        challenge.setTrenutnaRec(rec);
        qDebug()<<challenge.getTrenutnaRec();
        int condition = 0;
        const QVector<QLabel*>& polja = challenge.getPolja();
        QRegularExpression regex("background-color:\\s*([^;]+);");
        QRegularExpressionMatch match;
        QString colorString;

        challenge.ui->findChild<QPushButton*>("pbE")->click();
        challenge.ui->findChild<QPushButton*>("pbZ")->click();
        challenge.ui->findChild<QPushButton*>("pbZ")->click();
        challenge.ui->findChild<QPushButton*>("pbL")->click();
        challenge.ui->findChild<QPushButton*>("pbK")->click();
        challenge.ui->findChild<QPushButton*>("pbPogodi")->click();

        for(int i=0; i <5; i++)
            qDebug() << polja[i]->text();

        match = regex.match(polja[0]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;
        if(colorString == "#fec21c")
            condition++;

        match = regex.match(polja[1]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#a0b0c7")
            condition++;

        match = regex.match(polja[2]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#a0b0c7")
            condition++;

        match = regex.match(polja[3]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#fec21c")
            condition++;

        match = regex.match(polja[4]->styleSheet());
        colorString = match.captured(1).trimmed();
        qDebug() << colorString;

        if(colorString == "#a0b0c7")
            condition++;


        app.processEvents();

        REQUIRE(condition == 5);
        REQUIRE(challenge.getTrenutnaPozicija() == 5);
        REQUIRE(challenge.getPozicijaPocetnogSlova() == 5);



    }

    SECTION("Brisanje slova u jednom redu")
    {
        Timer timer;
        timer.start();
        ChallengeKalemegdan challenge(&timer,0,nullptr);
        challenge.setTrenutnaPozicija(4);
        int condition = 0;
        const QVector<QLabel*>& polja = challenge.getPolja();

        challenge.ui->findChild<QPushButton*>("pbIzbrisi")->click();
        challenge.ui->findChild<QPushButton*>("pbIzbrisi")->click();

        if(polja[4]->text()=="")
            condition++;
        if(polja[3]->text()=="")
            condition++;

        REQUIRE(challenge.getTrenutnaPozicija() == 2);
        REQUIRE(challenge.getTrenutnaPozicija() >= challenge.getPozicijaPocetnogSlova());
        REQUIRE(condition == 2);

    }


}
