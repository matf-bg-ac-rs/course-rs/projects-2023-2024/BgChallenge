#include "catch.hpp"
#include "../include/timer.h"
#include "../include/Challenge_test.h"
#include "../include/challengeMorzeovaAzbukaUI.h"
#include <QApplication>
#include <QSignalSpy>
#include <QDebug>
#include <QLineEdit>
#include <thread>
#include <chrono>
#include <QTimer>



TEST_CASE("Mrozeova azbuka izazov", "[class]")
{

    int argc = 0;
    char* argv[] = { nullptr };

    QApplication app(argc, argv);

    SECTION("Reagovanje na signale od sata i ispravno postavljanje pocetnih vrednosti polja")
    {

        Timer timer;
        timer.start();
        ChallengeMorzeovaAzbukaUI challenge(&timer,0,nullptr);

        QSignalSpy spy(&timer, SIGNAL(azurirajVreme(int)));

        emit timer.azurirajVreme(1);

        app.processEvents();


        REQUIRE(spy.count() > 0);
        REQUIRE(challenge.getRec() == "staklo");

    }

    SECTION("Klikom na dugme za paljenje svetla se pokrece timer")
    {
        Timer timer;
        timer.start();
        ChallengeMorzeovaAzbukaUI challenge(&timer,0,nullptr);

        challenge.ui->findChild<QPushButton*>("pbPonovi")->click();

        app.processEvents();

        REQUIRE(challenge.getCurrentIndex() == 0);


    }

    SECTION("Crtanje pozadine kada istekne tajmer kao simulacija paljenja i gasenja svetla"){
        Timer timer;
        timer.start();
        ChallengeMorzeovaAzbukaUI challenge(&timer,0,nullptr);

        challenge.ui->findChild<QPushButton*>("pbPonovi")->click();

        std::this_thread::sleep_for(std::chrono::milliseconds(400));

        app.processEvents();

        REQUIRE(challenge.getImagePath() == ":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_upaljeno.png");
        REQUIRE(challenge.getCurrentIndex() == 1);


    }


}
