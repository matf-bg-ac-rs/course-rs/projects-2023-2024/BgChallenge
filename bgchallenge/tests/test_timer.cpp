#include "catch.hpp"

#include "../include/mapa.h"
#include "../include/timer.h"
#include "../include/rezultat.h"
#include "../include/igrac.h"

#include<QString>
#include<QApplication>
#include<QTimer>
#include<QObject>
#include <chrono>
#include <thread>

TEST_CASE("Testiranje korektnosti klase Timer", "[timer]")
{
    int argc = 0;
    char **argv = {nullptr};

    QApplication app(argc, argv);

    SECTION("Provera da li tajmer zapocinje korektno da radi")
    {
        // arrange
        Timer timer(nullptr);

        // act
        timer.start();


        // assets
        REQUIRE(timer.isActive());
    }

    SECTION("Prover da li se tajmer zaustavlja korektno")
    {
        // arrange
        Timer timer(nullptr);

        // act
        timer.stop();

        // assert
        REQUIRE_FALSE(timer.isActive());
    }

    SECTION("Provera da li tajmer azurira vreme nakon cekanja od 2s") {
        // arrange
        Timer timer(nullptr);

        // act
        timer.start();
        std::this_thread::sleep_for(std::chrono::seconds(2));

        // assert
        REQUIRE(timer.getGameTime() >= 2);
    }

    SECTION("Provera da li tajmer restartuje vreme") {
        // arrange
        Timer timer(nullptr);

        // act
        timer.start();
        std::this_thread::sleep_for(std::chrono::seconds(2));
        timer.stop();

        // assert
        REQUIRE(timer.getGameTime() == 0);
    }

}
