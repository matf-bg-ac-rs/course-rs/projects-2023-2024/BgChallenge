#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "catch.hpp"
#include "../include/kviz.h"
#include "../include/Challenge_test.h"
#include "../include/timer.h"
#include "../forms/ui_kvizUI.h"
#include <QApplication>
#include <QStackedWidget>


class KvizTest {
public:
    static void on_Pitanje1OdgovorClicked(kviz& k) {
        k.onPitanje1OdgovorClicked();
    }
    static void on_Pitanje2OdgovorClicked(kviz& k) {
        k.onPitanje2OdgovorClicked();
    }
    static void on_Pitanje3OdgovorClicked(kviz& k) {
        k.onPitanje3OdgovorClicked();
    }

    static Ui::kviz* getUi(kviz& k) {
        return k.ui;
    }
};

TEST_CASE("Kviz Testovi", "[Kviz]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    SECTION("Inicijalizacija") {
        Timer timer;
        timer.start();
        kviz k(&timer, 0, nullptr);

        Ui::kviz *ui = KvizTest::getUi(k);
        REQUIRE(ui->stackedWidget->currentIndex() == 0);
    }

    SECTION("Prelazak na drugo pitanje") {
        Timer timer;
        timer.start();
        kviz k(&timer, 0, nullptr);

        KvizTest::on_Pitanje1OdgovorClicked(k);
        Ui::kviz *ui = KvizTest::getUi(k);
        REQUIRE(ui->stackedWidget->currentIndex() == 1);
    }
    SECTION("Prelazak na trece pitanje") {
        Timer timer;
        timer.start();
        kviz k(&timer, 0, nullptr);

        Ui::kviz *ui = KvizTest::getUi(k);
        ui->stackedWidget->setCurrentIndex(1);
        KvizTest::on_Pitanje2OdgovorClicked(k);
        REQUIRE(ui->stackedWidget->currentIndex() == 2);
    }
    SECTION("Prelazak na cetvrto pitanje") {
        Timer timer;
        timer.start();
        kviz k(&timer, 0, nullptr);

        Ui::kviz *ui = KvizTest::getUi(k);
        ui->stackedWidget->setCurrentIndex(2);
        KvizTest::on_Pitanje3OdgovorClicked(k);
        REQUIRE(ui->stackedWidget->currentIndex() == 3);
    }

    SECTION("Ponovi kviz i provera ako se upise tacno resenje") {
        Timer timer;
        timer.start();
        kviz k(&timer, 0, nullptr);

        Ui::kviz *ui = KvizTest::getUi(k);
        ui->stackedWidget->setCurrentIndex(3);
        k.ponovi = false;
        k.isCorrect = false;
        k.predjiDalje();
        if(k.ponovi){
        REQUIRE(KvizTest::getUi(k)->stackedWidget->currentIndex() == 0); // Povratak na prvo pitanje
        }else{
        REQUIRE(k.isCorrect == true);
        }
    }

}
