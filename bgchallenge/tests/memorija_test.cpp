#include "catch.hpp"
#include "../include/memorija.h"
#include "../include/timer.h"
#include <QApplication>
#include <QSignalSpy>

TEST_CASE("Memorija class tests", "[class]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    SECTION("Constructor and Initialization") {
        Timer timer;
        timer.start();
        Memorija memorija(&timer, 10, nullptr);

        REQUIRE(memorija.windowTitle() == "BgChallenge");
        REQUIRE(memorija.minimumWidth() == 1280);
        REQUIRE(memorija.minimumHeight() == 720);
    }

    SECTION("Button Click Handling") {
        Timer timer;
        timer.start();
        Memorija memorija(&timer, 10, nullptr);
        memorija.Tekstovi();

        QSignalSpy spyTimeoutNetacno(&memorija, SIGNAL(handleTimeoutNetacno()));
        QSignalSpy spyTimeoutTacno(&memorija, SIGNAL(handleTimeoutTacno()));


        QPushButton* mockButton1 = new QPushButton();
        QPushButton* mockButton2 = new QPushButton();


        emit mockButton1->clicked();
        emit mockButton2->clicked();

        REQUIRE(spyTimeoutNetacno.count() >= 0);
        REQUIRE(spyTimeoutTacno.count() >= 0);


        // Cleanup
        delete mockButton1;
        delete mockButton2;
    }

    SECTION("Testing button clicks and timer events") {

        Timer timer;
        timer.start();
        Memorija memorija(&timer, 10, nullptr);

        memorija.onBtnClicked();
        REQUIRE(memorija.getButtonPar().size() == 1);

        memorija.onBtnClicked();
        REQUIRE(memorija.getButtonPar().size() == 2);

        memorija.handleTimeoutNetacno();
        REQUIRE(memorija.getButtonPar().isEmpty());

        memorija.handleTimeoutTacno();
        REQUIRE(memorija.getButtonPar().isEmpty());
    }

}
