#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include<QLineEdit>
#include <QMouseEvent>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QSoundEffect>
#include "mapaUI.h"
#include "rezultat.h"




QT_BEGIN_NAMESPACE
namespace Ui { class mainwindow; }
QT_END_NAMESPACE

class mainwindow : public QWidget
{
    Q_OBJECT

public:
    mainwindow(QWidget *parent = nullptr);
    Rezultat *rez;
    ~mainwindow();

public slots:
    void ZapocniIgricuClicked();
    void PregledRezultataClicked();
    void PodesavanjaClicked();
    void NapustiIgricuClicked();

    void NatragNaGlavniMeniClicked();

    void NastaviIgricuClicked();
    void Kreni();

    void KlikMelodija();
    void JacinaPozadinseMelodije (int vrednost);

    void cestitajIgracu();
    void PrikaziUputstvo();


protected:
//    void resizeEvent(QResizeEvent *event) override;

    void KreniIgricu();
    bool eventFilter(QObject *obj, QEvent *event) override ;

signals:
    void clicked();


private:
    Ui::mainwindow *ui;
   // ChallengeMorzeovaAzbukaUI *u = nullptr;
    MapaUI *mapa = nullptr;
    QMediaPlayer *player;
    QAudioOutput *audioOutput;
    QMediaPlayer *player_klik=nullptr;
    QAudioOutput *audioOutput_klik=nullptr;
    void setUpTabWidges();
    void setUpBackground();
    void showLoadGame();
    QString formatSeconds(int seconds);
    bool postojiIgrac(const QString& ime);
};
#endif // MAINWINDOW_H
